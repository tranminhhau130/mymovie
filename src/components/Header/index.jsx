import React from "react";
//component
import ButtonCollape from "./ButtonCollapse";
import MenuNavbar from "./MenuNavbar";
import Auth from "./Auth";
//material UI
import Toolbar from "@material-ui/core/Toolbar";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

//logo
import logo from "../../asset/image/logo.png";
//scss
import "./style.scss";

const listMenu = ["Lịch Chiếu", "Cụm Rạp", "Tin Tức", "Ứng Dụng"];
const collapse = true;
function Header() {
  return (
    <div className="header">
      <Toolbar>
        <Link href="#">
          <img src={logo} alt="logo" className="header__logo"></img>
        </Link>
        <div className="header__destop">
          <MenuNavbar listMenu={listMenu}></MenuNavbar>
          <Typography
            align="right"
            display="block"
            noWrap
            className="header__destop--right"
          >
            <Auth></Auth>
          </Typography>
        </div>
        <div className="header__mobile">
          <ButtonCollape
            listMenu={listMenu}
            collapse={collapse}
          ></ButtonCollape>
        </div>
      </Toolbar>
    </div>
  );
}

export default Header;
