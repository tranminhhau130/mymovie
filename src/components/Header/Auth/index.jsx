import React, { Fragment } from "react";
import Button from "@material-ui/core/Button";

export default function Auth() {
  return (
    <Fragment>
      <Button size="small">Sign up</Button>
      <Button size="small">Sign in</Button>
    </Fragment>
  );
}
