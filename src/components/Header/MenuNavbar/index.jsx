import React from "react";
import Link from "@material-ui/core/Link";
import "./style.scss";

export default function MenuNavbar(props) {
  const collapse = props.collapse;
  function checkCollapse() {
    if (collapse) return "header__collapse";
    return "header__expand";
  }
  return (
    <div className={checkCollapse()}>
      {props.listMenu.map((item, index) => (
        <Link
          className={`${checkCollapse()}--item`}
          color="inherit"
          key={index}
          variant="body2"
          href="#"
          underline="none"
        >
          {item}
        </Link>
      ))}
    </div>
  );
}
