import React, { Fragment, useRef } from "react";
import Slider from "react-slick";
import CarouselItem from "./CarouselItem";
import ModalVideo from "./Modal";
import Link from "@material-ui/core/Link";
import "./style.scss";
const data = {
  nextSession: "https://tix.vn/app/assets/img/icons/next-session.png",
  backSession: "https://tix.vn/app/assets/img/icons/back-session.png",
  close: "https://tix.vn/app/assets/img/icons/close_black.png",
  carousel: [
    {
      img:
        "https://s3img.vcdn.vn/123phim/2020/11/chong-nguoi-ta-16053546067770.png",
      video: "https://www.youtube.com/embed/Vgb1uUmpQNU",
      icon: "https://tix.vn/app/assets/img/icons/play-video.png",
    },
    {
      img:
        "https://s3img.vcdn.vn/123phim/2020/11/dong-gia-1k-ve-khi-mua-ve-qua-tix-16043766276410.jpg",
      video: "https://www.youtube.com/embed/Vgb1uUmpQNU",
      icon: "https://tix.vn/app/assets/img/icons/play-video.png",
    },
    {
      img:
        "https://s3img.vcdn.vn/123phim/2020/11/gia-dinh-chan-to-phieu-luu-ky-bigfoot-family-p-16061896575573.png",
      video: "https://www.youtube.com/embed/Vgb1uUmpQNU",
      icon: "https://tix.vn/app/assets/img/icons/play-video.png",
    },
    {
      img:
        "https://s3img.vcdn.vn/123phim/2020/11/bi-mat-cua-gio-sneakshow-16063182906605.jpg",
      video: "https://www.youtube.com/embed/Vgb1uUmpQNU",
      icon: "https://tix.vn/app/assets/img/icons/play-video.png",
    },
  ],
};
const Carousel = (props) => {
  const ref = useRef({});

  const next = () => {
    ref.current.slickNext();
  };

  const previous = () => {
    ref.current.slickPrev();
  };

  function renderListItem() {
    return data.carousel.map((item, index) => {
      return (
        <Link className="carousel__slider--item">
          <CarouselItem item={item} key={`carousel${index}`}></CarouselItem>
        </Link>
      );
    });
  }
  const settings = {
    className: "carousel__slider",
    slidesToShow: 1,
    slidesToScroll: 1,
    // infinite: true,
    dots: true,
    // autoplay: true,
    // autoplaySpeed: 3000,
    arrows: false,
    dotsClass: "carousel__slider--dots",
  };
  return (
    <div className="carousel">
      <Slider ref={ref} {...settings}>
        {renderListItem()}
      </Slider>
      <button className="carousel__icon" onClick={previous}>
        <img className="carousel__icon--img" src={data.backSession} alt="" />
      </button>
      <button className="carousel__icon right" onClick={next}>
        <img className="carousel__icon--img" src={data.nextSession} alt="" />
      </button>
      {/* <ModalVideo></ModalVideo> */}
    </div>
  );
};

export default Carousel;
