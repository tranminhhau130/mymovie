import React, { Fragment } from "react";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";

export default function CarouselItem(props) {
  return (
    <Fragment>
      <img className="img" src={props.item.img} alt="index" />
      <Button className="icon">
        <img src={props.item.icon} alt="" />
      </Button>
    </Fragment>
  );
}
