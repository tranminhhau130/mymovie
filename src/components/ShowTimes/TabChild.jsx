import {
  Box,
  Link,
  makeStyles,
  Tab,
  Tabs,
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  TabScrollButton,
} from "@material-ui/core";
import React, { useState } from "react";
import { Fragment } from "react";

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}
const useStyles = makeStyles((theme) => ({
  rootChildren: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: 700,
  },
  tabs: {
    width: "25%",
    border: "1px solid #ebebec",
  },
  tabPanel: {
    flex: 1,
    border: "1px solid #ebebec",
    overflowX: "scroll",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  accordion: {
    padding: 15,
    boxShadow: "none",
    "&$expanded": { margin: 0 },
    "&::after": {
      content: "''",
      position: "absolute",
      bottom: 0,
      left: "50%",
      width: "calc(100% - 40px)",
      transform: "translateX(-50%)",
      borderBottom: "1px solid #ccc",
      borderColor: "rgba(238,238,238,.88)",
    },
    "&::before": {
      display: "none",
    },
  },
  accordionSummary: {
    margin: 0,
    alignItems: "center",
  },
  content: {
    // .MuiAccordionSummary-content.Mui-expanded
    "&$expanded": { margin: 0 },
    margin: 0,
    alignItems: "center",
  },
  expanded: {},
}));
export default function TabChild(props) {
  const classes = useStyles();
  const [value, setvalue] = useState(0);
  const handleChange = (event, newValue) => {
    setvalue(newValue);
  };
  const maHeThongRap = props.maHeThongRap;
  const cinemaListShowTime = props.cinemaListShowTime;

  function renderTabChildren() {
    return (
      <div className={classes.rootChildren}>
        <Tabs
          orientation="vertical"
          variant="scrollable"
          aria-label="Vertical tabs example"
          className={classes.tabs}
          value={value}
          onChange={handleChange}
          scrollButtons="off"
        >
          {renderTabChildrenDetail()}
        </Tabs>
        {renderTabPanelChildren(value)}
      </div>
    );
  }
  function renderTabChildrenDetail() {
    for (let item in cinemaListShowTime) {
      if (maHeThongRap === item && cinemaListShowTime[item].length) {
        return cinemaListShowTime[item].map((item) => {
          return (
            <Tab
              label={renderContentTab(item)}
              style={{ padding: 20, maxWidth: "100%", textAlign: "left" }}
              {...a11yProps(item.maCumRap)}
              key={item.maCumRap}
              className="showTimes__branch"
            ></Tab>
          );
        });
      }
    }
  }
  function renderContentTab(item) {
    return (
      <Fragment>
        <h2>{item.tenCumRap}</h2>
        <p>{item.diaChi}</p>
        <Link>Chi Tiết</Link>
      </Fragment>
    );
  }

  function renderShowTimeCinema(danhSachPhim) {
    return danhSachPhim.map((item, index) => {
      return (
        <Accordion
          classes={{
            root: classes.accordion,
            after: classes.after,
            before: classes.before,
            expanded: classes.expanded,
          }}
          defaultExpanded
          key={item.maPhim}
        >
          <AccordionSummary
            // makeStyle cho MuiAccordionSummary-root và MuiAccordionSummary-content
            // trong đó root bọc ngoài content
            classes={{
              root: classes.accordionSummary,
              content: classes.content,
              expanded: classes.expanded,
            }}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <img
              src={item.hinhAnh}
              style={{
                width: 50,
                float: "left",
                height: 50,
                objectFit: "cover",
              }}
              alt=""
            />
            <div
              style={{
                display: "block",
                overflow: "hidden",
                textOverflow: "ellipsis",
                flex: 1,
                marginLeft: 15,
              }}
            >
              <h5
                style={{
                  fontSize: 14,
                  color: "#000",
                  margin: 0,
                  lineHeight: "22px ",
                }}
              >
                {item.tenPhim}
              </h5>
            </div>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
              eget.
            </Typography>
          </AccordionDetails>
        </Accordion>
      );
    });
  }
  function renderTabPanelChildren(value) {
    for (let item in cinemaListShowTime) {
      if (maHeThongRap === item && cinemaListShowTime[item].length) {
        return cinemaListShowTime[item].map((item, index) => {
          return (
            <div
              role="tabpanel"
              hidden={value !== index}
              id={`vertical-tabpanel-${item.maCumRap}`}
              aria-labelledby={`vertical-tab-${item.maCumRap}`}
              className={classes.tabPanel}
            >
              {value === index && (
                <Typography>
                  {renderShowTimeCinema(item.danhSachPhim)}
                </Typography>
              )}
            </div>
          );
        });
      }
    }
  }
  return <Fragment>{renderTabChildren()}</Fragment>;
}
