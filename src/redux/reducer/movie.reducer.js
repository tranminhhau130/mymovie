let initialState = {};

const movieReducer = (state = initialState, action) => {
  let { type, payload } = action;
  switch (type) {
    default:
      return state;
  }
};
export default movieReducer;
