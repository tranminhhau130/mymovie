import {
  FETCHLISTSUCCESS,
  FETCHBRANCHSUCCESS,
  FETCHSHOWTIMESUCCESS,
} from "../constants/cinema.constant";

let initialState = {
  cinemaList: null,
  // cinemaListBranch: {
  //   BHDStar: {},
  //   CGV: {},
  //   CineStar: {},
  //   Galaxy: {},
  //   LotteCinima: {},
  //   MegaGS: {},
  // },
  cinemaListShowTime: {
    BHDStar: {},
    CGV: {},
    CineStar: {},
    Galaxy: {},
    LotteCinima: {},
    MegaGS: {},
  },
};

const cinemaReducer = (state = initialState, action) => {
  let { type, payload, id } = action;
  switch (type) {
    case FETCHLISTSUCCESS: {
      return { ...state, cinemaList: payload };
    }
    // case FETCHBRANCHSUCCESS: {
    //   let newList = { ...state.cinemaListBranch };
    //   for (let cinemaItem in newList) {
    //     if (cinemaItem === id) {
    //       newList[cinemaItem] = payload;
    //     }
    //   }
    //   return { ...state, cinemaListBranch: newList };
    // }
    case FETCHSHOWTIMESUCCESS: {
      let newList = { ...state.cinemaListShowTime };
      for (let cinemaItem in newList) {
        if (cinemaItem === id) {
          newList[cinemaItem] = payload;
        }
      }
      return { ...state, cinemaListShowTime: newList };
    }
    default:
      return state;
  }
};
export default cinemaReducer;
