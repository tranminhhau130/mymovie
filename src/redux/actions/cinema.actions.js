import {
  FETCHLISTSUCCESS,
  FETCHLISTFAILED,
  FETCHBRANCHSUCCESS,
  FETCHBRANCHFAILED,
  FETCHSHOWTIMESUCCESS,
  FETCHSHOWTIMEFAILED,
} from "../constants/cinema.constant";
import Axios from "axios";

// FETCH CINEMA LIST
export const fetchCinemaList = () => {
  return (dispatch) => {
    Axios.get(
      "https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinHeThongRap"
    )
      .then((res) => {
        dispatch(fetchCinemaListSuccess(res.data));
      })
      .catch((err) => {
        dispatch(fetchCinemaListFailed(err));
      });
  };
};

const fetchCinemaListSuccess = (cinemaList) => {
  return {
    type: FETCHLISTSUCCESS,
    payload: cinemaList,
  };
};
const fetchCinemaListFailed = (err) => {
  return {
    type: FETCHLISTFAILED,
    payload: err,
  };
};

// FETCH CINEMA BRANCH
// export const fetchCinemaBranch = (maHeThongRap) => {
//   return (dispatch) => {
//     Axios.get(
//       `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`
//     )
//       .then((res) => {
//         dispatch(fetchCinemaBranchSuccess(res.data, maHeThongRap));
//       })
//       .catch((err) => {
//         dispatch(fetchCinemaBranchFailed);
//         console.log(err);
//       });
//   };
// };

// const fetchCinemaBranchSuccess = (cinemaList, maHeThongRap) => {
//   return {
//     type: FETCHBRANCHSUCCESS,
//     payload: cinemaList,
//     id: maHeThongRap,
//   };
// };
// const fetchCinemaBranchFailed = (err) => {
//   return {
//     type: FETCHBRANCHFAILED,
//     payload: err,
//   };
// };

export const fetchCinemaShowTime = (maHeThongRap) => {
  return (dispatch) => {
    //set default value maNhom
    const maNhom = "GP01";
    Axios.get(
      `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${maHeThongRap}&maNhom=${maNhom}`
    )
      .then((res) => {
        dispatch(
          fetchCinemaShowTimeSuccess(res.data[0].lstCumRap, maHeThongRap)
        );
      })
      .catch((err) => {
        console.log(err);
        dispatch(fetchCinemaShowTimeFailed(err));
      });
  };
};
const fetchCinemaShowTimeSuccess = (data, maHeThongRap) => {
  return {
    type: FETCHSHOWTIMESUCCESS,
    payload: data,
    id: maHeThongRap,
  };
};
const fetchCinemaShowTimeFailed = (err) => {
  return {
    type: FETCHSHOWTIMEFAILED,
    payload: err,
  };
};
