const PREFIX = "CINEMA";

export const FETCHLISTSUCCESS = `FETCH_${PREFIX}_LIST_SUCESS`;
export const FETCHLISTFAILED = `FETCH_${PREFIX}_LIST_FAILED`;

export const FETCHBRANCHSUCCESS = `FETCH_${PREFIX}_BRANCH_SUCESS`;
export const FETCHBRANCHFAILED = `FETCH_${PREFIX}_BRANCH_FAILED`;

export const FETCHSHOWTIMESUCCESS = `FETCH_${PREFIX}_SHOWTIME_SUCESS`;
export const FETCHSHOWTIMEFAILED = `FETCH_${PREFIX}_SHOWTIME_FAILED`;
