import "./App.css";

import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { Fragment } from "react";
import Home from "./pages/Home";

function App() {
  return (
    <BrowserRouter>
      <Fragment>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
        </Switch>
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
